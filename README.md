# Symmetric Shadowcasting

![Alt text](shadowcasting.png)

## Description
This Roblox project uses a symmetric shadowcasting field of view algorithm for a grid-based map and movement.

Play it on <a href="https://www.roblox.com/games/17042591600/Symmetric-Shadowcasting-Showcase">Roblox</a>!
